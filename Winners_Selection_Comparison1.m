function [task_completion_percentage] = Winners_Selection_Comparison1(P,bids_orig,Values)
%winners_selection runs an auction to determine the set of winners 

bids=bids_orig;
[~,k]=size(P);
m=k-1;
S_tasks=zeros(1,m);   %most tasks must be done it must convert to all ones
S=[] ;                 %the winners of the auction
S_tasks_value=sum(S_tasks(:));
% ====================== CODE HERE ======================
a=100;
P_change=P;                            %%used to define P\S
P_change_value=sum(P_change(:));

while (P_change_value>0) && (a > 0)
    [u,~]=size(P_change);
    contribution=zeros(u,1);
    tot_contribution=zeros(u,1);
    for i=1:u
        for j=2:(m+1)
           if P_change(i,j) > S_tasks(1,(j-1))
              contribution(i)=contribution(i,1)+Values((j-1),2);
           end
        end
        tot_contribution(i)=contribution(i) - bids(i,2); %Vj(S)-bidsj
    end
    [a,s]=max(tot_contribution);
    if a>0
        S=[S;P_change(s,:)];
        S_tasks=P_change(s,2:end) | S_tasks; 
        bids(s,:)=[];
        P_change=setdiff(P_change,S,'rows');
        P_change_value=P_change(:,2:end);
        P_change_value=sum(P_change_value(:));
        S_tasks_value=sum(S_tasks(:));
    end 
end
task_completion_percentage=(S_tasks_value/m)*100;
end



% =============================================================

