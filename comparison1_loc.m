%Thesis code: comparison between new approach and Msensing wrt task
%             completion ratio 
%% Initialization
clear ; close all; clc

%% ========== Part 1: Comparing the percentage with changing number of auctions ====================

average_Msensing=zeros(1,10)
average_new_approach=zeros(1,10)
average_TSCM=zeros(1,10);
average_new_approach_with_rep=zeros(1,10);
for c=1:10
  task_completion_percentage_Msensing=zeros(1,c*100);
  task_completion_percentage_new_approach=zeros(1,c*100);
  task_completion_percentage_TSCM=zeros(1,c*100);
  task_completion_percentage_new_approach_with_rep=zeros(1,c*100);
  counter=0
for x=1:(c*100)

loc_users=randi([0,1000],100,2);
loc_tasks=randi([0,1000],100,2);
users=length(loc_users);
tasks=length(loc_tasks);
scatter(loc_users(:,1),loc_users(:,2),'r','o','filled','MarkerEdgeColor','k')
hold on
scatter(loc_tasks(:,1),loc_tasks(:,2),'y','s','filled','MarkerEdgeColor','k')
hold on
b=zeros(users,tasks);
V=[];
P=[];
for i=1:users
    th = 0:pi/50:2*pi;
    xunit = 30 * cos(th) + loc_users(i,1);
    yunit = 30 * sin(th) + loc_users(i,2);
    for j=1:tasks
        if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
            b(i,j)=1;
        end
    end
    h = plot(xunit, yunit,'k--');
    hold on
end
num_of_tasks_for_each_user=zeros(1,users);
for i=1:users    %each bidder should at least bid on one task
    num_of_tasks_for_each_user(i)=sum(b(i,:));
    if num_of_tasks_for_each_user(i)~=0  
        V = [V; b(i,:)];
    end
end
[users,tasks]=size(V);
P(:,1)=[1:users];
P(:,2:tasks+1)=V;
Values=horzcat([1:tasks]',randi([1,5],tasks,1))
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users)
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig'
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1 %user will make 1 task already so collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:)   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig
bids=bids_orig
[task_completion_percentage_Msensing(x)]= Winners_Selection_Comparison1(P,bids,Values);
[task_completion_percentage_new_approach(x)]= New_Approach_Comparison1(P,bids,Values,bid_by_task);
[task_completion_percentage_TSCM(x)]= TSCM_Winners_Selection_Comparison1(P,bids,Values,Reputations);
[task_completion_percentage_new_approach_with_rep(x)]= New_Approach_with_rep_Comparison1(P,bids,Values,Reputations,bid_by_task);

end
average_Msensing(c)=sum(task_completion_percentage_Msensing(:))/((100*c));
average_new_approach(c)=sum(task_completion_percentage_new_approach(:))/((100*c));
average_TSCM(c)=sum(task_completion_percentage_TSCM(:))/((100*c));
average_new_approach_with_rep(c)=sum(task_completion_percentage_new_approach_with_rep(:))/((100*c));


end

%% ========== Part 2: Plotting =======================
%task_comp_bar=horzcat(task_completion_percentage_Msensing',task_completion_percentage_new_approach')
%bar(task_comp_bar)
task_comp_bar=horzcat(average_Msensing',average_new_approach',average_TSCM',average_new_approach_with_rep')
bar(task_comp_bar)
xlabel({'Number of auctions','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')
figure
plot(average_Msensing,'r')
hold on
plot(average_new_approach,'g')
hold on
plot(average_TSCM,'b')
hold on
plot(average_new_approach_with_rep,'k')
xlabel({'Number of auctions','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')

%% ========== Part 3: Comparing the percentage with changing number of participants ====================
average_Msensing=zeros(1,10)
average_new_approach=zeros(1,10)
average_TSCM=zeros(1,10);
average_new_approach_with_rep=zeros(1,10);

for x=1:10
 task_completion_percentage_Msensing=zeros(1,5);
  task_completion_percentage_new_approach=zeros(1,5);
  task_completion_percentage_TSCM=zeros(1,5);
  task_completion_percentage_new_approach_with_rep=zeros(1,5);
  
for c=1:5
loc_users=randi([0,1000],x*100,2);
loc_tasks=randi([0,1000],100,2);
users=length(loc_users);
tasks=length(loc_tasks);
%scatter(loc_users(:,1),loc_users(:,2),'r','o','filled','MarkerEdgeColor','k')
%hold on
%scatter(loc_tasks(:,1),loc_tasks(:,2),'y','s','filled','MarkerEdgeColor','k')
%hold on
b=zeros(users,tasks);
V=[];
P=[];
for i=1:users
    th = 0:pi/50:2*pi;
    xunit = 30 * cos(th) + loc_users(i,1);
    yunit = 30 * sin(th) + loc_users(i,2);
    for j=1:tasks
        if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
            b(i,j)=1;
        end
    end
   % h = plot(xunit, yunit,'k--');
    %hold on
end
num_of_tasks_for_each_user=zeros(1,users);
for i=1:users    %each bidder should at least bid on one task
    num_of_tasks_for_each_user(i)=sum(b(i,:));
    if num_of_tasks_for_each_user(i)~=0  
        V = [V; b(i,:)];
    end
end
[users,tasks]=size(V);
P(:,1)=[1:users];
P(:,2:tasks+1)=V;
Values=horzcat([1:tasks]',randi([1,5],tasks,1));
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users);
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig';
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1 %user will make 1 task already so collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:);   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig;
bids=bids_orig;
[task_completion_percentage_Msensing(c)]= Winners_Selection_Comparison1(P,bids,Values);
[task_completion_percentage_new_approach(c)]= New_Approach_Comparison1(P,bids,Values,bid_by_task);
[task_completion_percentage_TSCM(c)]= TSCM_Winners_Selection_Comparison1(P,bids,Values,Reputations);
[task_completion_percentage_new_approach_with_rep(c)]= New_Approach_with_rep_Comparison1(P,bids,Values,Reputations,bid_by_task);

end
average_Msensing(x)=sum(task_completion_percentage_Msensing(:))/((5));
average_new_approach(x)=sum(task_completion_percentage_new_approach(:))/((5));
average_TSCM(x)=sum(task_completion_percentage_TSCM(:))/((5)-counter);
average_new_approach_with_rep(x)=sum(task_completion_percentage_new_approach_with_rep(:))/((5));

end

%% ========== Part 4: Plotting =======================
%task_comp_bar=horzcat(task_completion_percentage_Msensing',task_completion_percentage_new_approach')
%bar(task_comp_bar)
task_comp_bar=horzcat(average_Msensing',average_new_approach',average_TSCM',average_new_approach_with_rep')
bar(task_comp_bar)
xlabel({'Number of participants','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')
figure
plot(average_Msensing,'r')
hold on
plot(average_new_approach,'g')
hold on
plot(average_TSCM,'b')
hold on
plot(average_new_approach_with_rep,'k')
xlabel({'Number of participants','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')

%% ========== Part 5: Comparing the percentage with changing number of tasks ====================
average_Msensing=zeros(1,5)
average_new_approach=zeros(1,5)
average_TSCM=zeros(1,5);
average_new_approach_with_rep=zeros(1,5);

for x=1:5
  task_completion_percentage_Msensing=zeros(1,5);
  task_completion_percentage_new_approach=zeros(1,5);
  task_completion_percentage_TSCM=zeros(1,5);
  task_completion_percentage_new_approach_with_rep=zeros(1,5);
  counter=0
  
  for c=1:5
      check=1
      users=1000
      tasks=x*100
      b=randi([0,1],users,tasks);
      P=[];
      for i=1:5
        if sum(b(:,i))==0
            check=0
        end
      end
      P(:,1)=[1:users];
      P(:,2:tasks+1)=b;
      if (check~=0)
        Values=horzcat([1:tasks]',randi([1,5],tasks,1));
        Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
        Reputations=horzcat([1:users]',Rep_without_users);
        bids_orig=[[1:users];randi([1,10],users)];
        bids_orig=bids_orig';
        bid_by_all_task_orig=zeros(users,tasks);
        for i=1:tasks
            lb=Values(i,2)-2;
            if lb <= 0
               lb=1
            end
            ub=Values(i,2)+2;
            bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
        end
        bid_by_task_orig_without_users=bid_by_all_task_orig.*b;
        for j=1:users
            if sum(b(j,:))==1 %user will make 1 task already
                bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* b(j,:) ;  
            end
        end
        bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
        bid_by_task = bid_by_task_orig;
        bids=bids_orig;
        [task_completion_percentage_Msensing(c)]= Winners_Selection_Comparison1(P,bids,Values);
        [task_completion_percentage_new_approach(c)]= New_Approach_Comparison1(P,bids,Values,bid_by_task);
        [task_completion_percentage_TSCM(c)]= TSCM_Winners_Selection_Comparison1(P,bids,Values,Reputations);
        [task_completion_percentage_new_approach_with_rep(c)]= New_Approach_with_rep_Comparison1(P,bids,Values,Reputations,bid_by_task);

      else
        counter=counter+1;

      end

  end
  average_Msensing(x)=sum(task_completion_percentage_Msensing(:))/((5)-counter);
  average_new_approach(x)=sum(task_completion_percentage_new_approach(:))/((5)-counter);
  average_TSCM(x)=sum(task_completion_percentage_TSCM(:))/((5)-counter);
  average_new_approach_with_rep(x)=sum(task_completion_percentage_new_approach_with_rep(:))/((5)-counter);

end

%% ========== Part 6: Plotting =======================


task_comp_bar=horzcat(average_Msensing',average_new_approach',average_TSCM',average_new_approach_with_rep')
bar(task_comp_bar)
xlabel({'Number of tasks','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')
figure
plot(average_Msensing,'r')
hold on
plot(average_new_approach,'g')
hold on
plot(average_TSCM,'b')
hold on
plot(average_new_approach_with_rep,'k')
xlabel({'Number of tasks','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','b')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','b')
legend('Msensing','New approach reputation unaware','TSCM','New approach reputation aware','Location','north')
