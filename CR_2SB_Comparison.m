%Thesis code: Comparison between 
%             (2SB-RU and Msensing) and (2SB-RA and TSCM ) 
%             wrt Clearance Rate(CR)/ task completion rate.
%
% Reputation-Aware (RA)
% Reputation-Unaware (RU)
%% Initialization
clear ; close all; clc

%% ===== Part 1: Comparing the CR with changing number of auctions ======

average_CR_Msensing=zeros(1,10);
average_CR_2SB=zeros(1,10);
average_CR_TSCM=zeros(1,10);
average_CR_2SB_rep=zeros(1,10);
for c=1:10
  f=waitbar(c/10,'waiting ');
  CR_Msensing=zeros(1,c*100);
  CR_2SB=zeros(1,c*100);
  CR_TSCM=zeros(1,c*100);
  CR_2SB_rep=zeros(1,c*100);
  counter=0;
for x=1:(c*100)

check=0;
P=[];
while check==0
    loc_users=randi([0,1000],100,2);
    loc_tasks=randi([0,1000],100,2);
    users=length(loc_users);
    tasks=length(loc_tasks);
    %scatter(loc_users(:,1),loc_users(:,2),'r','o','filled','MarkerEdgeColor','k')
    %hold on
    %scatter(loc_tasks(:,1),loc_tasks(:,2),'y','s','filled','MarkerEdgeColor','k')
    %hold on
    %axis([0 1000 0 1000])
    %xlabel({'Distance in meters'},'FontSize',12,'FontWeight','bold','Color','b')
    %ylabel('Distance in meters','FontSize',12,'FontWeight','bold','Color','b')
    %legend('Participants','Tasks','Location','southeast')

    b=zeros(users,tasks);
    V=[];
    for i=1:users
        %th = 0:pi/50:2*pi;
        %xunit = 30 * cos(th) + loc_users(i,1);
        %yunit = 30 * sin(th) + loc_users(i,2);
        for j=1:tasks
            if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
                b(i,j)=1;
            end
        end
       % h = plot(xunit, yunit,'k--');
       % hold on
    end
    num_of_tasks_for_each_user=zeros(1,users);
    for i=1:users    %each bidder should at least bid on one task
        num_of_tasks_for_each_user(i)=sum(b(i,:));
        if num_of_tasks_for_each_user(i)~=0  
            V = [V; b(i,:)];
        end
    end
    [users,tasks]=size(V);
    if (users~=0) && (tasks~=0)
       check=1;
    end
end 
P(:,1)=[1:users];                                       %Auction participants
P(:,2:tasks+1)=V;                                       %Auction tasks values
Values=horzcat([1:tasks]',randi([1,5],tasks,1));
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users);
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig';
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1                 %user will make 1 task already so 
                                      %collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:);   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig;
bids=bids_orig;
[CR_Msensing(x)]= Msensing(P,bids,Values);
[CR_2SB(x)]= Two_SB_RU(P,bids,Values,bid_by_task);
[CR_TSCM(x)]= TSCM(P,bids,Values,Reputations);
[CR_2SB_rep(x)]= Two_SB_RA(P,bids,Values,Reputations,bid_by_task);

end
average_CR_Msensing(c)=sum(CR_Msensing(:))/((100*c));
average_CR_2SB(c)=sum(CR_2SB(:))/((100*c));
average_CR_TSCM(c)=sum(CR_TSCM(:))/((100*c));
average_CR_2SB_rep(c)=sum(CR_2SB_rep(:))/((100*c));


end

%% =================== Part 2: Plotting =======================
plot(average_CR_Msensing,'k-.o','LineWidth',1)
hold on
plot(average_CR_2SB,'m-s','LineWidth',1)
hold on
plot(average_CR_TSCM,'b--*','LineWidth',1)
hold on
plot(average_CR_2SB_rep,'r-','LineWidth',1)
xlabel({'Number of auctions','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','k')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','k')
legend('Msensing','2SB-RU','TSCM','2SB-RA','Location','northwest','NumColumns',1)

%% ==== Part 3: Comparing the CR with changing number of participants =====
average_CR_Msensing=zeros(1,10);
average_CR_2SB=zeros(1,10);
average_CR_TSCM=zeros(1,10);
average_CR_2SB_rep=zeros(1,10);

for x=1:10
 CR_Msensing=zeros(1,5);
  CR_2SB=zeros(1,5);
  CR_TSCM=zeros(1,5);
  CR_2SB_rep=zeros(1,5);
  
for c=1:5
check=0;
P=[];
while check==0
    loc_users=randi([0,1000],x*100,2);
    loc_tasks=randi([0,1000],100,2);
    users=length(loc_users);
    tasks=length(loc_tasks);
    b=zeros(users,tasks);
    V=[];
    for i=1:users
        %th = 0:pi/50:2*pi;
        %xunit = 30 * cos(th) + loc_users(i,1);
        %yunit = 30 * sin(th) + loc_users(i,2);
        for j=1:tasks
            if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
                b(i,j)=1;
            end
        end
       % h = plot(xunit, yunit,'k--');
       % hold on
    end
    num_of_tasks_for_each_user=zeros(1,users);
    for i=1:users    %each bidder should at least bid on one task
        num_of_tasks_for_each_user(i)=sum(b(i,:));
        if num_of_tasks_for_each_user(i)~=0  
            V = [V; b(i,:)];
        end
    end
    [users,tasks]=size(V);
    if (users~=0) && (tasks~=0)
       check=1;
    end
end 
P(:,1)=[1:users];
P(:,2:tasks+1)=V;
Values=horzcat([1:tasks]',randi([1,5],tasks,1));
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users);
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig';
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1 %user will make 1 task already so collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:);   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig;
bids=bids_orig;
[CR_Msensing(c)]= Msensing(P,bids,Values);
[CR_2SB(c)]= Two_SB_RU(P,bids,Values,bid_by_task);
[CR_TSCM(c)]= TSCM(P,bids,Values,Reputations);
[CR_2SB_rep(c)]= Two_SB_RA(P,bids,Values,Reputations,bid_by_task);

end
average_CR_Msensing(x)=sum(CR_Msensing(:))/((5));
average_CR_2SB(x)=sum(CR_2SB(:))/((5));
average_CR_TSCM(x)=sum(CR_TSCM(:))/((5)-counter);
average_CR_2SB_rep(x)=sum(CR_2SB_rep(:))/((5));

end

%% ================= Part 4: Plotting =======================

plot(average_CR_Msensing,'k-.o','LineWidth',1)
hold on
plot(average_CR_2SB,'m-s','LineWidth',1)
hold on
plot(average_CR_TSCM,'b--*','LineWidth',1)
hold on
plot(average_CR_2SB_rep,'r-','LineWidth',1)
xlabel({'Number of participants','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','k')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','k')
legend('Msensing','2SB-RU','TSCM','2SB-RA','Location','northwest','NumColumns',1)

%% ======= Part 5: Comparing the CR with changing number of tasks ========
average_CR_Msensing=zeros(1,5);
average_CR_2SB=zeros(1,5);
average_CR_TSCM=zeros(1,5);
average_CR_2SB_rep=zeros(1,5);

for x=1:5
  CR_Msensing=zeros(1,5);
  CR_2SB=zeros(1,5);
  CR_TSCM=zeros(1,5);
  CR_2SB_rep=zeros(1,5);
  counter=0;
  
for c=1:5
check=0;
P=[];
while check==0
    loc_users=randi([0,1000],1000,2);
    loc_tasks=randi([0,1000],x*100,2);
    users=length(loc_users);
    tasks=length(loc_tasks);
    b=zeros(users,tasks);
    V=[];
    for i=1:users
        %th = 0:pi/50:2*pi;
        %xunit = 30 * cos(th) + loc_users(i,1);
        %yunit = 30 * sin(th) + loc_users(i,2);
        for j=1:tasks
            if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
                b(i,j)=1;
            end
        end
       % h = plot(xunit, yunit,'k--');
       % hold on
    end
    num_of_tasks_for_each_user=zeros(1,users);
    for i=1:users    %each bidder should at least bid on one task
        num_of_tasks_for_each_user(i)=sum(b(i,:));
        if num_of_tasks_for_each_user(i)~=0  
            V = [V; b(i,:)];
        end
    end
    [users,tasks]=size(V);
    if (users~=0) && (tasks~=0)
       check=1;
    end
end 
P(:,1)=[1:users];
P(:,2:tasks+1)=V;
Values=horzcat([1:tasks]',randi([1,5],tasks,1));
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users);
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig';
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1 %user will make 1 task already so collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:);   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig;
bids=bids_orig;
[CR_Msensing(c)]= Msensing(P,bids,Values);
[CR_2SB(c)]= Two_SB_RU(P,bids,Values,bid_by_task);
[CR_TSCM(c)]= TSCM(P,bids,Values,Reputations);
[CR_2SB_rep(c)]= Two_SB_RA(P,bids,Values,Reputations,bid_by_task);
end
  average_CR_Msensing(x)=sum(CR_Msensing(:))/((5)-counter);
  average_CR_2SB(x)=sum(CR_2SB(:))/((5)-counter);
  average_CR_TSCM(x)=sum(CR_TSCM(:))/((5)-counter);
  average_CR_2SB_rep(x)=sum(CR_2SB_rep(:))/((5)-counter);

end

%% ================ Part 6: Plotting ==========================
plot(average_CR_Msensing,'k-.o','LineWidth',1.3)
hold on
plot(average_CR_2SB,'m-s','LineWidth',1.3)
hold on
plot(average_CR_TSCM,'b--*','LineWidth',1.3)
hold on
plot(average_CR_2SB_rep,'r','LineWidth',1.3)
xlabel({'Number of tasks','(in hundreds)'},'FontSize',12,'FontWeight','bold','Color','k')
ylabel('Percentage of task completion','FontSize',12,'FontWeight','bold','Color','k')
legend('Msensing','2SB-RU','TSCM','2SB-RA','Location','northwest','NumColumns',1)
