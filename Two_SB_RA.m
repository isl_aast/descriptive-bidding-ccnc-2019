function [task_completion_percentage2] = Two_SB_RA(P,bids_orig,Values,Reputations_orig,bid_by_task)
[n,k]=size(P);
m=k-1;
bids=bids_orig;
Max_allowed_payments=sum(Values(:,2));
Reputations=Reputations_orig;
% I need to return the following variable correctly 
other_S=[];
special_tasks_allocation=[];
Rep_S=[];
S_tasks=zeros(1,m);
% ===================== Primary Winners Selection ========================              
P_change=P;           %%used to define P\S
contribution=zeros(n,1);
tot_contribution=zeros(n,1);
S=[] ; 
  for i=1:n
    for j=2:(m+1)
        if P(i,j) > 0
            R_X_V=Reputations(i,2) * Values((j-1),2);  
            contribution(i)=contribution(i) + R_X_V;
        end
    end
    tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
  end
[a, s]=max(tot_contribution);

while (a>0)
    S=[S;P_change(s,:)];
    S_tasks= S_tasks | P_change(s,2:end);
    Rep_S=[Rep_S;Reputations(s,:)];
    P_change=setdiff(P_change,S,'rows');
    [r, t]=size(P_change);
    bids(s,:)=[];
    bid_by_task(s,:)=[];   %making it ready for the secondary users
    Reputations(s,:)=[];
    contribution_S=zeros(r,1);
    contribution=zeros(r,1);
    tot_contribution=zeros(r,1);
    for i=1:r
        S_plus_i=[P_change(i,:);S];
        Rep_S_plus_i=[Reputations(i,:);Rep_S];
        for j=2:t
            if S_plus_i(1,j) > 0
                column=sum(S_plus_i(:,j));
                R_X_V1=Rep_S_plus_i(:,2)' * S_plus_i(:,j);
                R_X_V2=R_X_V1* Values((j-1),2);
                avg_rep= R_X_V2/column;
                contribution(i)=contribution(i) + avg_rep;

                column_S=sum(S(:,j));
                if column_S ~=0;
                R_X_V1_S=Rep_S(:,2)' * S(:,j);
                R_X_V2_S=R_X_V1_S* Values((j-1),2);
                avg_rep_S= R_X_V2_S/column_S;
                contribution_S(i)=contribution_S(i) + avg_rep_S;
                end 
            end
        end
        contribution(i)= contribution(i)- contribution_S(i);
        tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
    end
    [a, s]=max(tot_contribution);
    compare=[];
    for i=1:r
        if tot_contribution(i)==a
            compare=[compare i];
        end
    end
    [~,siz]=size(compare);
    if siz~=1
    no_of_tasks=zeros(1,siz);
    for t=1:siz
        no_of_tasks(t)=sum(P_change(compare(t),2:end));
    end
    [~,best]=max(no_of_tasks);
    s=compare(best);
    end
end


S_tasks_value=sum(S_tasks);
task_completion_percentage1=(S_tasks_value/m)*100;
S=sortrows(S);
bids_winners=setdiff(bids_orig,bids,'rows');
Reputations_winners=setdiff(Reputations_orig,Reputations,'rows');
Reputations_winners=sortrows(Reputations_winners);
fprintf('task completion percentage1= %u%%' ,task_completion_percentage1)
[w,~]=size(S);
if S_tasks_value ~=m
  Payments=zeros(1,w);
% ================= Primary Winners Payment Determination==================
  if task_completion_percentage1==0  %if S=[] the code had an error so i put this if
    total_actual_primary_payments=0;
    REST=P;
    Rep_Sec=Reputations;
    REST_change=REST;
    remainder_budget=Max_allowed_payments-total_actual_primary_payments;
    bids_rest=bids_orig;

  else
    for i=1:w
      S_dash=setdiff(S,S(i,:),'rows');
      T=[];
      Rep_T=[];
      S_change=S_dash;      %S_dash\T
      bids=setdiff(bids_winners,bids_winners(i,:),'rows');
      [o,~]=size(S_change);
      T_value=sum(T(:));
      S_tasks_pay=zeros(1,m);
      S_dash_value=sum(S_dash(:));
      Reputations=Reputations_winners;
      Reputations_S_change=setdiff(Reputations,Reputations(i,:),'rows');
     %while (T_value ~= S_dash_value)
      for q=1:w  
          contribution_cu=0;
          contribution=zeros(o,1);
          contribution_T=0;
          tot_contribution=zeros(o,1);
          S_of_i_plus_T=[S(i,:);T];
          Rep_S_of_i_plus_T=[Reputations(i,:);Rep_T];

          for j=2:k
              if S(i,j) > 0   %common and unique
                 Si_column=sum(S_of_i_plus_T(:,j));
                 R_X_V1_Si=Rep_S_of_i_plus_T(:,2)' * S_of_i_plus_T(:,j);
                 R_X_V2_Si=R_X_V1_Si * Values((j-1),2);
                 avg_rep_Si= R_X_V2_Si/Si_column;
                 contribution_cu = contribution_cu + avg_rep_Si ;

                 if sum(T(:))~=0    %other unique
                   column_T=sum(T(:,j));  %without this line there is error entering column indices as T is empty
                   if column_T ~=0
                      R_X_V1_T=Rep_T(:,2)' * T(:,j);
                      R_X_V2_T=R_X_V1_T * Values((j-1),2);
                      avg_rep_T= R_X_V2_T/column_T;
                      contribution_T=contribution_T + avg_rep_T;
                   end 
                 end
              end
         end
         contribution_special =contribution_cu - contribution_T;

         if (T_value ~= S_dash_value)
           for z=1:o
             T_plus_z=[S_change(z,:);T];
             Rep_T_plus_z=[Reputations_S_change(z,:);Rep_T];

             for j=2:k
               if T_plus_z(1,j) > 0
                column=sum(T_plus_z(:,j));
                R_X_V1=Rep_T_plus_z(:,2)' * T_plus_z(:,j);
                R_X_V2=R_X_V1 * Values((j-1),2);
                avg_rep= R_X_V2/column;
                contribution(z)= contribution(z) + avg_rep;

               end
             end
             contribution(z)= contribution(z)- contribution_T;
             tot_contribution(z)=contribution(z)-((bids(z,2))/Reputations_S_change(z,2)); %Vj(S)-bidsj
           end
           [a,s]=max(tot_contribution);
           S_tasks_pay= S_change(s,2:end) | S_tasks_pay ;
           T=[T;S_change(s,:)];
           Rep_T=[Rep_T;Reputations_S_change(s,:)];
           bids(s,:)=[];
           S_change=setdiff(S_change,T,'rows');
           [o,~]=size(S_change);
           T_value=sum(T(:));
           Reputations_S_change(s,:)=[];
         else
           a=0;
         end

         Payments(i)=max(Payments(i),(contribution_special-a));
      end
    end
    % ==================== Secondry Winners Selection ========================

    total_actual_primary_payments=sum(Payments);
    remainder_budget=Max_allowed_payments-total_actual_primary_payments;
    not_done=[] ;
    for j=1:m
        if S_tasks(j)==0
           not_done=[not_done j];
           fprintf('task number %u is not done \n ',j);
        end
    end
    [~,r]=size(not_done);
    REST=setdiff(P,S,'rows');
    bids_rest_all=setdiff(bids_orig,bids_winners,'rows');
    Rep_Sec1=setdiff(Reputations_orig,Reputations_winners,'rows'); %needs filtering
    Rep_Sec=[] ; %will have the secondary participants that can do our missing tasks
    bid_by_task1=[];
    REST_change=[];
    bids_rest=[];
    [y,~]=size(REST);
    for i=1:y    %check that the rest do the tasks  we want
        check_rest=0;
        for q=1:r
            j=not_done(q)+1;
            if REST(i,j)==1
                check_rest=1;
            end
        end
        if check_rest==1
            REST_change=[REST_change;REST(i,:)];
            bids_rest=[bids_rest;bids_rest_all(i,:)];
            Rep_Sec=[Rep_Sec;Rep_Sec1(i,:)];
            bid_by_task1=[bid_by_task1;bid_by_task(i,:)];
        end
    end
    bid_by_task=bid_by_task1;
 end
  Rep_S_sec=[];
  bid_by_task_S=[];
  
 

  while (sum(S_tasks_value)~=m) && (sum(REST_change(:)>0))
      [u,~]=size(REST_change);
      REST_change_modified=[];
      bid_by_task_modified=[];
      bids_rest_modified=[];
      for g=1:u   %this loop makes all already done tasks disapear from coming participants
       for k=2:(m+1)
           if REST_change(g,k) == S_tasks(1,(k-1))
              REST_change(g,k)=0 ;
              bid_by_task(g,k)=0;
           end
       end
       num_tasks_each_user(g)=sum(REST_change(g,2:end));
       if num_tasks_each_user(g)~=0  
           REST_change_modified=[REST_change_modified;REST_change(g,:)];
           bid_by_task_modified=[bid_by_task_modified;bid_by_task(g,:)];
           bids_rest_modified=[bids_rest_modified;bids_rest(g,:)];  
       end
      end
      REST_change=REST_change_modified;
      bid_by_task=bid_by_task_modified;
      bids_rest=bids_rest_modified;
      [y,~]=size(REST_change);
      contribution_sec_total=zeros(y,1);
      contribution_sec=zeros(y,1);
      contribution_S_sec=zeros(y,1);
      if y~=0
      special_task_needed=[REST_change(:,1) zeros(y,m)];
      bids_sum=zeros(y,1);
      for i=1:y
         S_plus_i_sec=[REST_change(i,:);other_S];
         Rep_S_plus_i_sec=[Rep_Sec(i,:);Rep_S_sec];
         for j=2:(m+1)
            if S_plus_i_sec(1,j) > S_tasks(1,(j-1));
               column_sec=sum(S_plus_i_sec(:,j));
               R_X_V1_sec=Rep_S_plus_i_sec(:,2)' * S_plus_i_sec(:,j);
               bids_sum(i)= bids_sum(i) + bid_by_task(i,j);
               R_X_V2_sec=R_X_V1_sec * Values((j-1),2);
               avg_rep_sec= R_X_V2_sec/column_sec;
               special_task_needed(i,j)=1;
               contribution_sec(i)=contribution_sec(i) + avg_rep_sec;

               if sum(other_S(:))~=0    %other unique
                  column_S_sec=sum(other_S(:,j));
                  if column_S_sec ~=0
                     R_X_V1_S_sec=Rep_S_sec(:,2)' * other_S(:,j);
                     R_X_V2_S_sec=R_X_V1_S_sec * Values((j-1),2);
                     avg_rep_S_sec= R_X_V2_S_sec/column_S_sec;
                     contribution_S_sec(i)=contribution_S_sec(i) + avg_rep_S_sec;
                  end
               end
            end
         end
         contribution_sec_total(i)= contribution_sec(i)- contribution_S_sec(i) - (bids_sum(i)/Rep_S_plus_i_sec(1,2));
      end
      [a, ~]=max(contribution_sec_total);
      compare=[];
        for i=1:y
            if (contribution_sec_total(i)==a) && (((Rep_Sec(i,2)*remainder_budget)-bids_sum(i))>=0)
                compare=[compare i];
            end
        end
        [~,siz]=size(compare);
        if siz~=0
        no_of_tasks=zeros(1,siz);
        for t=1:siz
            no_of_tasks(t)=sum(REST_change(compare(t),2:end));
        end
        [~,best]=max(no_of_tasks);
        s=compare(best);
        
      %if a>=0  % or equal is so important
        other_S=[other_S;REST_change(s,:)];
        Rep_S_sec=[Rep_S_sec;Rep_Sec(s,:)];
        bid_by_task_S=[bid_by_task_S;bid_by_task(s,:)];
        S_tasks=REST_change(s,2:end) | S_tasks;
        remainder_budget=(Rep_Sec(s,2)*remainder_budget)-(bids_sum(s));
        special_tasks_allocation=[special_tasks_allocation;special_task_needed(s,:)];
        REST_change=setdiff(REST_change,other_S,'rows');
        bid_by_task(s,:)=[];
        bids_rest(s,:)=[];
        Rep_Sec(s,:)=[];
        S_tasks_value=sum(S_tasks(:));
       else
        REST_change=[];
       end
      else
        REST_change=[];  
      end 
  end
  task_completion_percentage2=(S_tasks_value/m)*100;
  fprintf('task completion percentage2= %u%%' ,task_completion_percentage2)
else
    task_completion_percentage2=100;
end 
end 

% =============================================================