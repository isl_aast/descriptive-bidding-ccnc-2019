function [task_completion_percentage] = New_Approach_Comparison1(P,bids_orig,Values,bid_by_task)
%  P=[1,0,0,0,1,0,0;2,0,1,0,1,1,1;3,1,0,1,0,0,1;4,0,1,1,0,1,1]
%  Values=[1,1;2,1;3,3;4,1;5,4;6,2]
%  bid_by_task=[1,0,0,0,1,0,0;2,0,1,0,2,2,1;3,2,0,1,0,0,4;4,0,3,2,0,4,2]
%  bids_orig=[1,1;2,4;3,10;4,3]
bids=bids_orig;
[~,k]=size(P);
m=k-1;
Max_allowed_payments=sum(Values(:,2));
S_tasks=zeros(1,m);   %most tasks must be done it must convert to all ones
S=[] ;                 %the winners of the auction
other_S=[];
special_tasks_allocation=[];
% ====================== Primary Winners Selection ======================
a=100;
P_change=P;                            %%used to define P\S
P_change_value=sum(P_change(:));

while (P_change_value>0) && (a > 0)
    [u,~]=size(P_change);
    contribution=zeros(u,1);
    tot_contribution=zeros(u,1);
    for i=1:u
        for j=2:(m+1)
            if P_change(i,j) > S_tasks(1,(j-1))
               contribution(i)=contribution(i,1)+Values((j-1),2);
            end
        end
        tot_contribution(i)=contribution(i)-bids(i,2); %Vj(S)-bidsj
    end
    [a,s]=max(tot_contribution);
    compare=[];
    for i=1:u
        if tot_contribution(i)==a
            compare=[compare i];
        end
    end
    [~,siz]=size(compare);
    if siz~=1
    no_of_tasks=zeros(1,siz);
    for t=1:siz
        no_of_tasks(t)=sum(P_change(compare(t),2:end));
    end
    [~,best]=max(no_of_tasks);
    s=compare(best);
    end
    if a>0 %for the first time only because we put an arbitrary a at the begining
        S =[S;P_change(s,:)];
        S_tasks=P_change(s,2:end) | S_tasks;
        bids(s,:)=[];
        bid_by_task(s,:)=[];
        P_change=setdiff(P_change,S,'rows');
        P_change_value=P_change(:,2:end);
        P_change_value=sum(P_change_value(:));
    end 
end
S = sortrows(S);
bids_winners=setdiff(bids_orig,bids,'rows');
S_tasks_value=sum(S_tasks(:));

if sum(S_tasks(:))~=m 

% ================= Primary Winners Payment Determination==================
[u,~]=size(S);
Payments=zeros(1,u);
for i=1:u
  S_dash=setdiff(S,S(i,:),'rows');
  T=[];
  %S_change=setdiff(S_dash,T,'rows');
  S_change=S_dash;      %S_dash\T
  bids=setdiff(bids_winners,bids_winners(i,:),'rows');
  [o,~]=size(S_change);
  T_value=sum(T(:));
  S_tasks_pay=zeros(1,m); %this is different from the original S_task (this is changed according to each user)
  S_dash_value=sum(S_dash(:));
 
 %while (T_value ~= S_dash_value)
 for q=1:u 
  
  contribution=zeros(o,1);
  tot_contribution=zeros(o,1);
  contribution_special=0;
     for j=2:k
        if S(i,j) > S_tasks_pay(1,(j-1))
           contribution_special=contribution_special+Values((j-1),2);
        end
     end
   if (T_value ~= S_dash_value)    
     for z=1:o
         for j=2:k
             if S_change(z,j) > S_tasks_pay(1,(j-1))
                contribution(z)=contribution(z)+Values((j-1),2);
             end
         end
         tot_contribution(z)=contribution(z)-bids(z,2); %Vj(S)-bidsj
     end
     [a,s]=max(tot_contribution);
     S_tasks_pay=S_change(s,2:end) | S_tasks_pay ;
     T=[T;S_change(s,:)];
     bids(s,:)=[];
     S_change=setdiff(S_change,T,'rows');
     [o,~]=size(S_change);
     T_value=sum(T(:)); 
   else
       a=0;
   end
   
     Payments(i)=max(Payments(i),(contribution_special-a));
end
end

% ==================== Secondry Winners Selection ========================
total_actual_primary_payments=sum(Payments);
remainder_budget=Max_allowed_payments-total_actual_primary_payments;
not_done=[]; 
    for j=1:m
        if S_tasks(j)==0
           not_done=[not_done j];
           %fprintf('task number %u is not done \n ',j);
        end
    end
    [~,r]=size(not_done);
    REST=P_change;
    bids_rest_all=setdiff(bids_orig,bids_winners,'rows');
    bid_by_task1=[];
    REST_change=[];
    bids_rest=[];
    [y,~]=size(REST);
    for i=1:y    %check that the rest do the tasks  we want
        check_rest=0;
        for q=1:r
            j=not_done(q)+1;
            if REST(i,j)==1
                check_rest=1;
            end
        end
        if check_rest==1
            REST_change=[REST_change;REST(i,:)];
            bids_rest=[bids_rest;bids_rest_all(i,:)];
            bid_by_task1=[bid_by_task1;bid_by_task(i,:)];
        end
    end
   bid_by_task=bid_by_task1;
  
   while (sum(REST_change(:)>0))
       [w,~]=size(REST_change);
       REST_change_modified=[]; %de el hn7ot feha el kwaieseen
       bid_by_task_modified=[];
       bids_rest_modified=[];
       for g=1:w   %this loop makes all already done tasks disapear from coming participants
        for k=2:(m+1)
           if REST_change(g,k) == S_tasks(1,(k-1))
              REST_change(g,k)=0 ;
              bid_by_task(g,k)=0;
           end
        end
        num_tasks_each_user(g)=sum(REST_change(g,2:end));
        if num_tasks_each_user(g)~=0  
           REST_change_modified=[REST_change_modified;REST_change(g,:)];
           bids_rest_modified=[bids_rest_modified;bids_rest(g,:)];
           bid_by_task_modified=[bid_by_task_modified;bid_by_task(g,:)];
        end
       end
       REST_change=REST_change_modified;
       bid_by_task=bid_by_task_modified;
       bids_rest=bids_rest_modified;
       [u,~]=size(REST_change);
       if u~=0
       contribution=zeros(u,1);
       special_task_needed=[REST_change(:,1) zeros(u,m)];
       bids_sum=zeros(u,1);
       for i=1:u 
          for j=2:(m+1)
             if REST_change(i,j) > S_tasks(1,(j-1));
                contribution(i)=contribution(i,1) + Values((j-1),2);
                bids_sum(i)= bids_sum(i)+ bid_by_task(i,j);
                special_task_needed(i,j)=1;
             end
          end
          %if bids_sum(i) < bids_rest(i,2)
             %bids_sum(i)=bids_rest(i,2);
         %end
         contribution(i)=contribution(i,1) -bids_sum(i);
       end
       [b,~]=max(contribution);
       compare=[];
        for i=1:u
            if (contribution(i)==b) && (remainder_budget - bids_sum(i)>=0)
                compare=[compare i];
            end
        end
        [~,siz]=size(compare);
        if siz~=0
        no_of_tasks=zeros(1,siz);
        for t=1:siz
            no_of_tasks(t)=sum(REST_change(compare(t),2:end));
        end
        [~,best]=max(no_of_tasks);
        r=compare(best);
        
       %if (remainder_budget-bids_sum(r))>=0
         other_S=[other_S;REST_change(r,:)];
         S_tasks=REST_change(r,2:end) | S_tasks;
         special_tasks_allocation=[special_tasks_allocation;special_task_needed(r,:)];
         REST_change=setdiff(REST_change,other_S,'rows');
         bid_by_task(r,:)=[];
         S_tasks_value=sum(S_tasks(:));
         remainder_budget=remainder_budget-bids_sum(r);
         bids_rest(r,:)=[];
         % payment_other_winners=[payment_other_winners;b];
       %elseif (b < 0) && (b + remainder_budget>=0)
           %remainder_budget=remainder_budget+b;
          % remainder_budget=remainder_budget-bids_sum(r)
           %if remainder_budget >=0  %else ykhrog bara coz kol el gai b3do
                                        % asghar mno kman
          %     other_S=[other_S;REST_change(r,:)]
           %    S_tasks=REST_change(r,2:end) | S_tasks;
            %   special_tasks_allocation=[special_tasks_allocation;special_task_needed(r,:)];
          % end
          % REST_change(r,:)=[];   %just nsheel el user da l2eno manfe3sh 
                            %el budget bta3etna masta7mltsh tedfa3lo
          % bids_rest(r,:)=[];
          % bid_by_task(r,:)=[];
          % S_tasks_value=sum(S_tasks(:));
       else
           fprintf('no one is interested in the remaining tasks or bids for individual tasks are more than their value')
           deficit= b + remainder_budget;
           REST_change=[];
       end
       end
       
   end
end  
task_completion_percentage=(S_tasks_value/m)*100;
end
% =============================================================

