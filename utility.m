clear ; close all; clc

%% ========== Part 1: Comparing the Utility ====================
average_Msensing_utility=zeros(1,10);
average_2SB_utility=zeros(1,10);
average_TSCM_utility=zeros(1,10);
average_2SB_rep_utility=zeros(1,10);
for c=1:10
  f=waitbar(c/10,'waiting ')
  utility_Msensing=zeros(1,c*100);
  utility_new_approach=zeros(1,c*100);
  utility_TSCM=zeros(1,c*100);
  utility_new_approach_with_rep=zeros(1,c*100);
for x=1:(c*100)

check=0;
P=[];
while check==0
    loc_users=randi([0,1000],100,2);
    loc_tasks=randi([0,1000],100,2);
    users=length(loc_users);
    tasks=length(loc_tasks);
    b=zeros(users,tasks);
    V=[];
    for i=1:users
        for j=1:tasks
            if ((loc_tasks(j,1)- loc_users(i,1))^2 + (loc_tasks(j,2)-loc_users(i,2))^2) <= 30^2
                b(i,j)=1;
            end
        end
    end
    num_of_tasks_for_each_user=zeros(1,users);
    for i=1:users    %each bidder should at least bid on one task
        num_of_tasks_for_each_user(i)=sum(b(i,:));
        if num_of_tasks_for_each_user(i)~=0  
            V = [V; b(i,:)];
        end
    end
    [users,tasks]=size(V);
    if (users~=0) && (tasks~=0)
       check=1;
    end
end 
P(:,1)=[1:users];
P(:,2:tasks+1)=V;
Values=horzcat([1:tasks]',randi([1,5],tasks,1));
Rep_without_users=0.6+ (0.9-0.6) .* rand(users,1);
Reputations=horzcat([1:users]',Rep_without_users);
bids_orig=[[1:users];randi([1,10],1,users)];
bids_orig=bids_orig';
bid_by_all_task_orig=zeros(users,tasks);
for i=1:tasks
    lb=Values(i,2)-2;
    if lb <= 0
       lb=1;
    end
    ub=Values(i,2)+2;
    bid_by_all_task_orig(:,i)=randi([lb,ub],1,users);
end
bid_by_task_orig_without_users=bid_by_all_task_orig.* V;
for j=1:users
    if sum(V(j,:))==1 %user will make 1 task already so collective bid equals bid by task
        bid_by_task_orig_without_users(j,:)=bids_orig(j,2).* V(j,:);   
    end
end
bid_by_task_orig=horzcat([1:users]',bid_by_task_orig_without_users);
bid_by_task = bid_by_task_orig;
bids=bids_orig;
[utility_Msensing(x)]= MSensing_utility(P,bids,Values);
[utility_new_approach(x)]= Two_SB_RU_utility(P,bids,Values,bid_by_task);
[utility_TSCM(x)]= TSCM_utility(P,bids,Values,Reputations);
[utility_new_approach_with_rep(x)]= Two_SB_RA_utility(P,bids,Values,Reputations,bid_by_task);
end
average_Msensing_utility(c)=sum(utility_Msensing(:))/((100*c));
average_2SB_utility(c)=sum(utility_new_approach(:))/((100*c));
average_TSCM_utility(c)=sum(utility_TSCM(:))/((100*c));
average_2SB_rep_utility(c)=sum(utility_new_approach_with_rep(:))/((100*c));


end


%% ================= Part 2: Plotting =======================


plot(average_Msensing_utility,'k-','LineWidth',1.3)
hold on
plot(average_2SB_utility,'r-o','LineWidth',1.3)
hold on
plot(average_TSCM_utility,'b--*','LineWidth',1.3)
hold on
plot(average_2SB_rep_utility,'m-s','LineWidth',1.3)

xlabel({'Number of auctions','(in hundreds)'},'FontSize',10,'FontWeight','bold','Color','k')
ylabel('Average User Utility','FontSize',10,'FontWeight','bold','Color','k')
legend('Msensing','2SB-RU','TSCM','2SB-RA','Location','northwest','NumColumns',1)

