function [task_completion_percentage] = TSCM(P,bids_orig,Values,Reputations_orig)
bids=bids_orig;
Reputations=Reputations_orig;
[u,k]=size(P);
m=k-1;
S=[] ;                 %the winners of the auction
Rep_S=[];
S_tasks=zeros(1,m);
% ====================== CODE HERE ======================
P_change=P;           %%used to define P\S
contribution=zeros(u,1);
tot_contribution=zeros(u,1);

  for i=1:u
    for j=2:(m+1)
        if P(i,j) > 0
            R_X_V=Reputations(i,2) * Values((j-1),2);  
            contribution(i)=contribution(i) + R_X_V;
        end
    end
    tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
  end
[a, s]=max(tot_contribution);

while (a>0)
    S=[S;P_change(s,:)];
    S_tasks=S_tasks | P_change(s,2:end);
    Rep_S=[Rep_S;Reputations(s,:)];
    P_change=setdiff(P_change,S,'rows');
    [r, t]=size(P_change);
    bids(s,:)=[];
    Reputations(s,:)=[];
    contribution_S=zeros(r,1);
    contribution=zeros(r,1);
    tot_contribution=zeros(r,1);
    for i=1:r
        S_plus_i=[P_change(i,:);S];
        Rep_S_plus_i=[Reputations(i,:);Rep_S];
        for j=2:t
            if S_plus_i(1,j) > 0
               column=sum(S_plus_i(:,j));
               R_X_V1=Rep_S_plus_i(:,2)' * S_plus_i(:,j);
               R_X_V2=R_X_V1* Values((j-1),2);
               avg_rep= R_X_V2/column;
               contribution(i)=contribution(i) + avg_rep;

               column_S=sum(S(:,j));
               if column_S ~=0
                  R_X_V1_S=Rep_S(:,2)' * S(:,j);
                  R_X_V2_S=R_X_V1_S* Values((j-1),2);
                  avg_rep_S= R_X_V2_S/column_S;
                  contribution_S(i)=contribution_S(i) + avg_rep_S;
               end 
            end
        end
        contribution(i)= contribution(i)- contribution_S(i);
        tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
    end
    [a, s]=max(tot_contribution);
end
S_tasks_value=sum(S_tasks);
task_completion_percentage=(S_tasks_value/m)*100;
end
% =============================================================

