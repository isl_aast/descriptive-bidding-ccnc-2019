function [task_completion_percentage3] = bidpertaskrep(P,Values,Reputations,bid_by_task_orig)
% P=[1,0,0,0,0,1,1;2,1,0,0,1,1,0;3,1,1,1,0,0,1;4,0,1,0,1,1,0]
% Values=[1,2;2,2;3,4;4,5;5,1;6,4];
% bid_by_task_orig=[1,0,0,0,0,3,3;2,3,0,0,7,2,0;3,4,1,2,0,0,5;4,0,4,0,7,2,0]
% % Reputations_orig=[1,0.658739394308020;2,0.897707691953978;3,0.840678470929287;4,0.727268012970992];
% Reputations=[1,0.6587;2,0.897;3,0.84;4,0.727];

% P=[1,0,1,0,1,1,1;2,0,1,0,1,1,1;3,1,1,0,1,0,1;4,1,0,1,0,1,0]
% Values=[1,3;2,3;3,2;4,4;5,3;6,4]
% bids=[1,8;2,8;3,7;4,2]
% bid_by_task_orig=[1,0,5,0,5,4,6;2,0,2,0,4,3,6;3,2,5,0,2,0,3;4,5,0,3,0,5,0]
% Reputations=[1,0.69;2,0.64;3,0.74;4,0.7]
[~,k]=size(P);
m=k-1;
Reputations_orig=Reputations;
% I need to return the following variable correctly 
special_tasks_allocation_2nd_trial=[];
bid_by_task_S_trial2=[];
P_change2=P;
S_tasks2=zeros(1,m);   %all tasks must be done it must convert to all ones
S_tasks2_value=sum(S_tasks2(:));
other_S_trial2=[];
Rep_S_trial2=[];
Max_allowed_payments=sum(Values(:,2));
remainder_budget=Max_allowed_payments
   while (sum(S_tasks2_value)~=m) && (sum(P_change2(:)>0))
      [u,~]=size(P_change2);
      P_change2_modified=[];
      bid_by_task_orig_modified=[];
      for g=1:u   %this loop makes all already done tasks disapear from coming participants
         num_tasks_each_user=0;
          for l=2:(m+1)
            if P_change2(g,l) == S_tasks2(1,(l-1))
               P_change2(g,l)=0;
               bid_by_task_orig(g,l)=0;
            end
         end
         num_tasks_each_user=sum(P_change2(g,2:end));
         if (num_tasks_each_user > 0)  
           P_change2_modified=[P_change2_modified;P_change2(g,:)];
           bid_by_task_orig_modified=[bid_by_task_orig_modified;bid_by_task_orig(g,:)];
         end
      end
     P_change2=P_change2_modified;
     bid_by_task_orig=bid_by_task_orig_modified;
     [y,~]=size(P_change2);
     if y~=0
     contribution_trial2=zeros(y,1);
     contribution_S_trial2=zeros(y,1);
      %special_task_needed_2nd_trial=[P_change2(:,1) zeros(y,m)]
      bids_sum_trial2=zeros(y,1);
      for i=1:y
         S_plus_i_trial2=[P_change2(i,:);other_S_trial2];
         Rep_S_plus_i_trial2=[Reputations_orig(i,:);Rep_S_trial2];
         for j=2:(m+1)
            if S_plus_i_trial2(1,j) > S_tasks2(1,(j-1))
               column_trial2=sum(S_plus_i_trial2(:,j));
               R_X_V1_trial2=Rep_S_plus_i_trial2(:,2)' * S_plus_i_trial2(:,j);
               bids_sum_trial2(i)= bids_sum_trial2(i) + bid_by_task_orig(i,j);
               R_X_V2_trial2=R_X_V1_trial2 * Values((j-1),2);
               avg_rep_trial2= R_X_V2_trial2/column_trial2;
               %special_task_needed_2nd_trial(i,j)=1
               contribution_trial2(i)=contribution_trial2(i) + avg_rep_trial2;
 
               if sum(other_S_trial2(:))~=0    %other unique
                  column_S_trial2=sum(other_S_trial2(:,j));
                  if column_S_trial2 ~=0
                     R_X_V1_S_trial2=Rep_S_trial2(:,2)' * other_S_trial2(:,j);
                     R_X_V2_S_trial2=R_X_V1_S_trial2 * Values((j-1),2);
                     avg_rep_S_trial2= R_X_V2_S_trial2/column_S_trial2;
                     contribution_S_trial2(i)=contribution_S_trial2(i) + avg_rep_S_trial2;
                  end
               end
            end
         end
         contribution_trial2(i)= contribution_trial2(i)- contribution_S_trial2(i) - (bids_sum_trial2(i)/Rep_S_plus_i_trial2(1,2))
      end
      [a, ~]=max(contribution_trial2);
      compare=[];
        for i=1:y
            %if (contribution_trial2(i)==a) && (((Reputations_orig(i,2)*remainder_budget)-(bids_sum_trial2(i)/Reputations_orig(i,2)))>=0)
            if (contribution_trial2(i)==a) && (((Reputations_orig(i,2)*remainder_budget)-(bids_sum_trial2(i)))>=0)
            
                compare=[compare i];
            end
        end
        [~,siz]=size(compare);
        if siz~=0
        no_of_tasks=zeros(1,siz);
        for t=1:siz
            no_of_tasks(t)=sum(P_change2(compare(t),2:end));
        end
        [~,best]=max(no_of_tasks);
        s=compare(best);
        
      %if a>=0  % or equal is so important
        other_S_trial2=[other_S_trial2;P_change2(s,:)];
        Rep_S_trial2=[Rep_S_trial2;Reputations_orig(s,:)];
        bid_by_task_S_trial2=[bid_by_task_S_trial2;bid_by_task_orig(s,:)];
        S_tasks2=P_change2(s,2:end) | S_tasks2;
%        remainder_budget=(Reputations_orig(s,2)*remainder_budget)-(bids_sum_trial2(s)/Reputations_orig(s,2));
        remainder_budget=(Reputations_orig(s,2)*remainder_budget)-(bids_sum_trial2(s))   
        %special_tasks_allocation_2nd_trial=[special_tasks_allocation_2nd_trial;special_task_needed_2nd_trial(s,:)]
        P_change2=setdiff(P_change2,other_S_trial2,'rows');
        bid_by_task_orig(s,:)=[];
        Reputations_orig(s,:)=[];
        S_tasks2_value=sum(S_tasks2(:));
        %elseif (a < 0) && ((a + remainder_budget) >=0 )
       %elseif (a < 0) && ((a + (Rep_S_plus_i_trial2(1,2) * remainder_budget))>=0)
        %remainder_budget=(Rep_S_plus_i_trial2(1,2) * remainder_budget)+a;
        %remainder_budget=remainder_budget+a
        %remainder_budget=remainder_budget-bids_sum_trial2(s)
        %if remainder_budget >=0
        %other_S_trial2=[other_S_trial2;P_change2(s,:)]
        %Rep_S_trial2=[Rep_S_trial2;Reputations_orig(s,:)]
        %bid_by_task_S_trial2=[bid_by_task_S_trial2;bid_by_task_orig(s,:)]
        %S_tasks2=P_change2(s,2:end) | S_tasks2
        %special_tasks_allocation_2nd_trial=[special_tasks_allocation_2nd_trial;special_task_needed_2nd_trial(s,:)]
        %P_change2=setdiff(P_change2,other_S_trial2,'rows')
        %end
        %P_change2(s,:)=[];  %just nsheel el user da l2eno manfe3sh 
                            %el budget bta3etna masta7mltsh tedfa3lo
        %bid_by_task_orig(s,:)=[];
        %Reputations_orig(s,:)=[];
        %S_tasks2_value=sum(S_tasks2(:))
       else
        deficit= a + remainder_budget;
        P_change2=[];
           
        end 
       else
    P_change2=[];
     end
   end
   
other_S_trial2=sortrows(other_S_trial2);
%special_tasks_allocation_2nd_trial=sortrows(special_tasks_allocation_2nd_trial)
bid_by_task_orig=sortrows(bid_by_task_orig);
task_completion_percentage3=(S_tasks2_value/m)*100;
%this_percentage2=task_completion_percentage3 
%task_completion_percentage_2=[task_completion_percentage_2 this_percentage2]
%to have same name as comparison 
%without reputation, in new approach task completion perccenatge 1 was the
%TSCM and percentage 2 was after adding the per task bid
%but here this percentage 1 is the whole algorithm percentage 2 is per task
%bid from the begining and comparing these two together


end

