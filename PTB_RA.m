function [task_completion_percentage3] = PTB_RA(P,Values,Reputations,bid_by_task_orig)
[~,k]=size(P);
m=k-1;
Reputations_orig=Reputations;
% I need to return the following variable correctly 
bid_by_task_S_trial2=[];
P_change2=P;
S_tasks2=zeros(1,m);   %all tasks must be done it must convert to all ones
S_tasks2_value=sum(S_tasks2(:));
other_S_trial2=[];
Rep_S_trial2=[];
Max_allowed_payments=sum(Values(:,2));
remainder_budget=Max_allowed_payments;
   while (sum(S_tasks2_value)~=m) && (sum(P_change2(:)>0))
      [u,~]=size(P_change2);
      P_change2_modified=[];
      bid_by_task_orig_modified=[];
      for g=1:u   %this loop makes all already done tasks disapear from coming participants
          for l=2:(m+1)
            if P_change2(g,l) == S_tasks2(1,(l-1))
               P_change2(g,l)=0;
               bid_by_task_orig(g,l)=0;
            end
         end
         num_tasks_each_user=sum(P_change2(g,2:end));
         if (num_tasks_each_user > 0)  
           P_change2_modified=[P_change2_modified;P_change2(g,:)];
           bid_by_task_orig_modified=[bid_by_task_orig_modified;bid_by_task_orig(g,:)];
         end
      end
     P_change2=P_change2_modified;
     bid_by_task_orig=bid_by_task_orig_modified;
     [y,~]=size(P_change2);
     if y~=0
     contribution_trial2=zeros(y,1);
     contribution_S_trial2=zeros(y,1);
      bids_sum_trial2=zeros(y,1);
      for i=1:y
         S_plus_i_trial2=[P_change2(i,:);other_S_trial2];
         Rep_S_plus_i_trial2=[Reputations_orig(i,:);Rep_S_trial2];
         for j=2:(m+1)
            if S_plus_i_trial2(1,j) > S_tasks2(1,(j-1))
               column_trial2=sum(S_plus_i_trial2(:,j));
               R_X_V1_trial2=Rep_S_plus_i_trial2(:,2)' * S_plus_i_trial2(:,j);
               bids_sum_trial2(i)= bids_sum_trial2(i) + bid_by_task_orig(i,j);
               R_X_V2_trial2=R_X_V1_trial2 * Values((j-1),2);
               avg_rep_trial2= R_X_V2_trial2/column_trial2;
               contribution_trial2(i)=contribution_trial2(i) + avg_rep_trial2;
 
               if sum(other_S_trial2(:))~=0    %other unique
                  column_S_trial2=sum(other_S_trial2(:,j));
                  if column_S_trial2 ~=0
                     R_X_V1_S_trial2=Rep_S_trial2(:,2)' * other_S_trial2(:,j);
                     R_X_V2_S_trial2=R_X_V1_S_trial2 * Values((j-1),2);
                     avg_rep_S_trial2= R_X_V2_S_trial2/column_S_trial2;
                     contribution_S_trial2(i)=contribution_S_trial2(i) + avg_rep_S_trial2;
                  end
               end
            end
         end
         contribution_trial2(i)= contribution_trial2(i)- contribution_S_trial2(i) - (bids_sum_trial2(i)/Rep_S_plus_i_trial2(1,2));
      end
      [a, ~]=max(contribution_trial2);
      compare=[];
        for i=1:y
            if (contribution_trial2(i)==a) && (((Reputations_orig(i,2)*remainder_budget)-(bids_sum_trial2(i)))>=0)
                compare=[compare i];
            end
        end
        [~,siz]=size(compare);
        if siz~=0
        no_of_tasks=zeros(1,siz);
        for t=1:siz
            no_of_tasks(t)=sum(P_change2(compare(t),2:end));
        end
        [~,best]=max(no_of_tasks);
        s=compare(best);
        other_S_trial2=[other_S_trial2;P_change2(s,:)];
        Rep_S_trial2=[Rep_S_trial2;Reputations_orig(s,:)];
        bid_by_task_S_trial2=[bid_by_task_S_trial2;bid_by_task_orig(s,:)];
        S_tasks2=P_change2(s,2:end) | S_tasks2;
        remainder_budget=(Reputations_orig(s,2)*remainder_budget)-(bids_sum_trial2(s));   
        P_change2=setdiff(P_change2,other_S_trial2,'rows');
        bid_by_task_orig(s,:)=[];
        Reputations_orig(s,:)=[];
        S_tasks2_value=sum(S_tasks2(:));
        else
        deficit= a + remainder_budget;
        P_change2=[];
           
        end 
       else
    P_change2=[];
     end
   end
   
other_S_trial2=sortrows(other_S_trial2);
bid_by_task_orig=sortrows(bid_by_task_orig);
task_completion_percentage3=(S_tasks2_value/m)*100;


end

