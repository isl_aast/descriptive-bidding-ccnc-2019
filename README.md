#Descriptive Bidding CCNC 2019
This is the code of the two descriptive bidding procedures proposed in:
1. M.E.Gendy, A.Al-Kabbany, E.F. Badran, "Maximizing Clearance Rate of Reputation-aware Auctions in Mobile Crowdsensing," In CCNC, 2019 Proceedings IEEE, January 2019.
2. M. E. Gendy, A. Al-Kabbany, E. F. Badran, "Maximizing Clearance Rate of Reputation-aware Auctions in Mobile Crowdsensing,"  CoRR, abs/1810.05774, 2018.
If you will use this code in your research, please cite the aforementioned articles.

We refer to the two descriptive bidding procedures as the two-stage descriptive bidding and the totally-descritpive bidding (or simply the descritpive bidding) procedures. The former starts by selecting the primary winners based on their collective bids, and then starts another round for selcting secondary winners using their descriptive bids. The latter proceeds with the descriptive bids only, from the beginning.