function [utility] = TSCM_utility(P,bids_orig,Values,Reputations_orig)
%Payment_Determination determines the payment for each winner found
%from the winners_selection function
bids=bids_orig;
Reputations=Reputations_orig;
[u,k]=size(P);
m=k-1;
S=[] ;                 %the winners of the auction
Rep_S=[];
S_tasks=zeros(1,m);
% ====================== CODE HERE ======================
P_change=P;           %%used to define P\S
%P_change_value=sum(P_change(:));
contribution=zeros(u,1);
tot_contribution=zeros(u,1);

  for i=1:u
    for j=2:(m+1)
        if P(i,j) > 0
            %contribution(i)=contribution(i,1)+(Values((j-1),2)*Reputations(i,2)/sigma(j-1));
            R_X_V=Reputations(i,2) * Values((j-1),2);  
            contribution(i)=contribution(i) + R_X_V;
        end
    end
    tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
  end
[a, s]=max(tot_contribution);

while (a>0)
    S=[S;P_change(s,:)];
    S_tasks=S_tasks | P_change(s,2:end);
    Rep_S=[Rep_S;Reputations(s,:)];
    P_change=setdiff(P_change,S,'rows');
    [r, t]=size(P_change);
    bids(s,:)=[];
    Reputations(s,:)=[];
    %[r t]=size(S_plus_i);
    contribution_S=zeros(r,1);
    contribution=zeros(r,1);
    tot_contribution=zeros(r,1);
    for i=1:r
        S_plus_i=[P_change(i,:);S];
        Rep_S_plus_i=[Reputations(i,:);Rep_S];
        for j=2:t
            if S_plus_i(1,j) > 0
               column=sum(S_plus_i(:,j));
               R_X_V1=Rep_S_plus_i(:,2)' * S_plus_i(:,j);
               %R_X_V2=sum(R_X_V1(:)) 
               R_X_V2=R_X_V1* Values((j-1),2);
               avg_rep= R_X_V2/column;
               contribution(i)=contribution(i) + avg_rep;

               column_S=sum(S(:,j));
               if column_S ~=0
                  R_X_V1_S=Rep_S(:,2)' * S(:,j);
                  %R_X_V2_S=sum(R_X_V1_S(:)) 
                  R_X_V2_S=R_X_V1_S* Values((j-1),2);
                  avg_rep_S= R_X_V2_S/column_S;
                  contribution_S(i)=contribution_S(i) + avg_rep_S;
               end 
            end
        end
        contribution(i)= contribution(i)- contribution_S(i);
        tot_contribution(i)=contribution(i)-((bids(i,2))/Reputations(i,2));
    end
    [a, s]=max(tot_contribution);
end
% =============================================================
S=sortrows(S);
bids_winners=setdiff(bids_orig,bids,'rows');
Reputations_winners=setdiff(Reputations_orig,Reputations,'rows');
Reputations_winners=sortrows(Reputations_winners);

% Initialize some useful values
[u,k]=size(S);
if u~=0
m=k-1;
Reputations=Reputations_winners;
% I need to return the following variable correctly 
Payments=zeros(1,u);   %all winners should be paid larger or equal to their bids

% ====================== CODE HERE ======================
 
for i=1:u
  S_dash=setdiff(S,S(i,:),'rows');
  T=[];
  Rep_T=[];
  S_change=S_dash;      %S_dash\T
  bids=setdiff(bids_winners,bids_winners(i,:),'rows');
  [o,~]=size(S_change);
  T_value=sum(T(:));
  S_tasks=zeros(1,m);
  S_dash_value=sum(S_dash(:));
  Reputations_S_change=setdiff(Reputations,Reputations(i,:),'rows');
 %while (T_value ~= S_dash_value)
  
 for q=1:u  
  contribution_special=0;
  contribution_cu=0;
  contribution=zeros(o,1);
  contribution_T=0;
  tot_contribution=zeros(o,1);
  S_of_i_plus_T=[S(i,:);T];
  Rep_S_of_i_plus_T=[Reputations(i,:);Rep_T];

     for j=2:k
        if S(i,j) > 0   %common and unique
            Si_column=sum(S_of_i_plus_T(:,j));
            R_X_V1_Si=Rep_S_of_i_plus_T(:,2)' * S_of_i_plus_T(:,j);
            %R_X_V2=sum(R_X_V1(:)) 
            R_X_V2_Si=R_X_V1_Si * Values((j-1),2);
            avg_rep_Si= R_X_V2_Si/Si_column;
            contribution_cu = contribution_cu + avg_rep_Si ;

           
            if sum(T(:))~=0    %other unique
               column_T=sum(T(:,j));  %without this line there is error entering column indices as T is empty
               if column_T ~=0
                  R_X_V1_T=Rep_T(:,2)' * T(:,j);
                  %R_X_V2_S=sum(R_X_V1_S(:)) 
                  R_X_V2_T=R_X_V1_T * Values((j-1),2);
                  avg_rep_T= R_X_V2_T/column_T;
                  contribution_T=contribution_T + avg_rep_T;
               end 
            end  
        end
     end
     contribution_special =contribution_cu - contribution_T;
    
   if (T_value ~= S_dash_value)
   
    
     for z=1:o
       T_plus_z=[S_change(z,:);T];
       Rep_T_plus_z=[Reputations_S_change(z,:);Rep_T];
      
       
       for j=2:k
          if T_plus_z(1,j) > 0
            column=sum(T_plus_z(:,j));
            R_X_V1=Rep_T_plus_z(:,2)' * T_plus_z(:,j);
            %R_X_V2=sum(R_X_V1(:)) 
            R_X_V2=R_X_V1 * Values((j-1),2);
            avg_rep= R_X_V2/column;
            contribution(z)= contribution(z) + avg_rep;
        
           % if sum(T(:))~=0
              % column_T=sum(T(:,j))  %without this line there is error entering column indices as T is empty
               %if column_T ~=0
                % R_X_V1_T=Rep_T(:,2)' * T(:,j)
                 %%R_X_V2_S=sum(R_X_V1_S(:)) 
                 %R_X_V2_T=R_X_V1_T * Values((j-1),2)
                 %avg_rep_T= R_X_V2_T/column_T
                 %contribution_T(z)=contribution_T(z) + avg_rep_T
               %end 
            %else
             %   contribution_T(z)=0
            %end
          end
       end
     contribution(z)= contribution(z)- contribution_T;
     tot_contribution(z)=contribution(z)-((bids(z,2))/Reputations_S_change(z,2)); %Vj(S)-bidsj
     end
     [a,s]=max(tot_contribution);
     S_tasks= S_change(s,2:end) | S_tasks; 
     T=[T;S_change(s,:)];
     Rep_T=[Rep_T;Reputations_S_change(s,:)];
     bids(s,:)=[];
     S_change=setdiff(S_change,T,'rows');
     [o,~]=size(S_change);
     T_value=sum(T(:));
     Reputations_S_change(s,:)=[];
   else
       a=0;
   end
   
     Payments(i)=max(Payments(i),(contribution_special-a));
end
end
utility=Payments-(bids_winners(:,2))';
utility=sum(utility(:))/u;
else
    utility=0;
end
end
% =============================================================
