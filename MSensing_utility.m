function [utility] = MSensing_utility(P,bids_orig,Values)
bids=bids_orig;
[~,k]=size(P);
m=k-1;
S_tasks=zeros(1,m);   %most tasks must be done it must convert to all ones
S=[] ;                 %the winners of the auction
% ====================== CODE HERE ======================
a=100;
P_change=P;                            %%used to define P\S
P_change_value=sum(P_change(:));

while (P_change_value>0) && (a > 0)
    [u,~]=size(P_change);
    contribution=zeros(u,1);
    tot_contribution=zeros(u,1);
    for i=1:u
        for j=2:(m+1)
           if P_change(i,j) > S_tasks(1,(j-1))
              contribution(i)=contribution(i,1)+Values((j-1),2);
           end
        end
        tot_contribution(i)=contribution(i) - bids(i,2); %Vj(S)-bidsj
    end
    [a,s]=max(tot_contribution);
    if a>0
        S=[S;P_change(s,:)];
        S_tasks=P_change(s,2:end) | S_tasks; 
        bids(s,:)=[];
        P_change=setdiff(P_change,S,'rows');
        P_change_value=P_change(:,2:end);
        P_change_value=sum(P_change_value(:));
    end 
end
S=sortrows(S);
bids_winners=setdiff(bids_orig,bids,'rows');

[u,k]=size(S);
if u~=0
m=k-1;
Payments=zeros(1,u);   %all winners should be paid larger or equal to their bids
% ====================== CODE HERE ======================
for i=1:u
  S_dash=setdiff(S,S(i,:),'rows');
  T=[];
  %S_change=setdiff(S_dash,T,'rows');
  S_change=S_dash;      %S_dash\T
  bids=setdiff(bids_winners,bids_winners(i,:),'rows');
  [o,~]=size(S_change);
  T_value=sum(T(:));
  S_tasks=zeros(1,m);
  S_dash_value=sum(S_dash(:));
 
 %while (T_value ~= S_dash_value)
  for q=1:u  
      contribution=zeros(o,1);
      tot_contribution=zeros(o,1);
      contribution_special=0;
      for j=2:k
        if S(i,j) > S_tasks(1,(j-1))
           contribution_special=contribution_special+Values((j-1),2);
        end
      end
      if (T_value ~= S_dash_value)    
         for z=1:o
             for j=2:k
                 if S_change(z,j) > S_tasks(1,(j-1))
                    contribution(z)=contribution(z)+Values((j-1),2);
                 end
             end
             tot_contribution(z)=contribution(z)-bids(z,2); %Vj(S)-bidsj
         end
         [a,s]=max(tot_contribution);
         S_tasks=S_change(s,2:end) | S_tasks; 
         T=[T;S_change(s,:)];
         bids(s,:)=[];
         S_change=setdiff(S_change,T,'rows');
         [o,~]=size(S_change);
         T_value=sum(T(:));
      else
         a=0;
      end
      Payments(i)=max(Payments(i),(contribution_special-a));
  end
end
utility=Payments-(bids_winners(:,2))';
utility=sum(utility(:))/u;
else
    utility=0;
end
end
% =============================================================
