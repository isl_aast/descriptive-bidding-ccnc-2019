function [task_completion_percentage] = PTB_RU(P,Values,bid_by_task_orig)
[~,k]=size(P);
m=k-1;
Max_allowed_payments=sum(Values(:,2));
S_tasks=zeros(1,m);                        %all tasks must be done it must convert to all ones
S=[] ;                                     %the winners of the auction 
bid_by_task=bid_by_task_orig;
remainder_budget=Max_allowed_payments;
special_tasks_allocation=[];
% ====================== Primary Winners Selection ======================
P_change=P;                           %%used to define P\S
P_change_value=sum(P_change(:));
while (P_change_value>0)
%while (P_change_value>0) && (a > 0)
  [w,~]=size(P_change);
  P_change_modified=[];
  bid_by_task_modified=[];
  for g=1:w   %this loop makes all already done tasks disapear from coming participants
      for l=2:(m+1)
        if P_change(g,l) == S_tasks(1,(l-1))
           P_change(g,l)=0; 
           bid_by_task(g,l)=0;
        end
      end
     num_tasks_each_user=sum(P_change(g,2:end));
     if (num_tasks_each_user > 0)  
        P_change_modified=[P_change_modified;P_change(g,:)];
        bid_by_task_modified=[bid_by_task_modified;bid_by_task(g,:)];

     end
  end
  P_change=P_change_modified;
  bid_by_task=bid_by_task_modified;
  [u,~]=size(P_change);
  if u~=0
  contribution=zeros(u,1);
  bids_sum=zeros(u,1);
  special_task_needed=[P_change(:,1) zeros(u,m)];
  for i=1:u
      for j=2:(m+1)
        if P_change(i,j) > S_tasks(1,(j-1))
           contribution(i)=contribution(i,1) + Values((j-1),2);
           special_task_needed(i,j)=1;
           bids_sum(i)=bids_sum(i)+bid_by_task(i,j);
        end
      end
      contribution(i)=contribution(i,1) - bids_sum(i);
  end
    [a,~]=max(contribution);
    compare=[];
    for i=1:u
        if (contribution(i)==a)  && (remainder_budget-bids_sum(i)>=0)     
            compare=[compare i];
        end
    end
    [~,siz]=size(compare);
    if siz~=0
        no_of_tasks=zeros(1,siz);
        for t=1:siz
            no_of_tasks(t)=sum(P_change(compare(t),2:end));
        end
        [~,best]=max(no_of_tasks);
        s=compare(best);
    %end
    %if a>=0 ; %for the first time only because we put an arbitrary a at the begining
        S =[S;P_change(s,:)];
        S_tasks=P_change(s,2:end) | S_tasks;
        bid_by_task(s,:)=[];
        special_tasks_allocation=[special_tasks_allocation;special_task_needed(s,:)];
        P_change=setdiff(P_change,S,'rows');
        P_change_value=P_change(:,2:end);
        P_change_value=sum(P_change_value(:));
        remainder_budget=remainder_budget- bids_sum(s);
     else
            P_change=[];
            P_change_value=0;

     
    end
  else
    P_change_value=0;
 end
end
S = sortrows(S);
special_tasks_allocation=sortrows(special_tasks_allocation);
S_tasks_value=sum(S_tasks(:));
task_completion_percentage=(S_tasks_value/m)*100;
end